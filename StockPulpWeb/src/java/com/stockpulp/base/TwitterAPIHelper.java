/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stockpulp.base;
import java.net.*;
import java.io.*;
import javax.net.ssl.*;
import org.json.simple.*;

/**
 *
 * @author tomastezak
 */
public class TwitterAPIHelper {
    
    public static String restApiRequest(String restPathUrl, String bearerToken) throws IOException {
        HttpsURLConnection connection = null;
				
	try {
		URL url = new URL(restPathUrl); 
		connection = (HttpsURLConnection) url.openConnection();           
		connection.setDoOutput(true);
		connection.setDoInput(true); 
		connection.setRequestMethod("GET"); 
		connection.setRequestProperty("Host", "api.twitter.com");
		connection.setRequestProperty("User-Agent", "StockPulpDev");
		connection.setRequestProperty("Authorization", "Bearer " + bearerToken);
		connection.setUseCaches(false);
			
                String result= TwitterConnectionHelper.readResponse(connection);
		
		if (result!= null) {
			return result;
		}
		return new String();
	}
	catch (MalformedURLException e) {
		throw new IOException("Invalid endpoint URL specified.", e);
	}
	finally {
		if (connection != null) {
			connection.disconnect();
		}
	}
        
    }
    
    
    public static String parseJSONResult(String json){
        JSONArray obj = (JSONArray)JSONValue.parse(json);
			
		if (obj != null) {
			String tweet = ((JSONObject)obj.get(0)).get("text").toString();

			return (tweet != null) ? tweet : "";
		}
		return new String();
    
    }
    
}
